---
title: Golem Overview
subtitle: Analysis of Golem
date: 2017-11-02
tags: ["Golem"]
---


# What is Golem?

Golem (GNT) is the cryptocurrency behind the Golem network, a decentralised, 
peer-to-peer, distributed processing network allowing you to lease your unused
or idle computing power for others to use, similar to Berkeley's BOINC and 
Amazon Web Services. The Golem developers plan for users to be able to use the 
Golem network for a wide range of tasks, including:
  - Machine Learning training
  - Stock Market Analysis
  - CryptoCurrency Mining
  - Graphical Rendering
  - Scientific Research

As of November 2017, Golem is still in its Alpha stages, only runs on a test net
and only supports one of the features stated above, Graphical rendering. It has 
the 38th highest coin market cap ~($158,246924), with a trading volume of around
~$2,400,000 a day. Golem is based on the Ethereum network through an Ethereum
Contract, and thus its transactions and how they're verified are done through
the Ethereum blockchain.

Golem plans to release 4 different types of "Golems," or pieces of software 
handling different tasks of the Golem Network:

- The Brass Golem, which will solely focus on distributed computer rendering.
- The Clay Golem, a new golem with expanded capabilities compared to the Brass 
  Golem that will be able to handle more features stated above.
- The Stone Golem, a golem that will allow users to upload their own third party
  programs onto the golem network and allow others to rent out these programs. 
  Will continue to optimize and develop the tasks stated above for Golem usage.
- The Iron Golem, which will be a more robust and secure version of the Stone
  Golem capable, and will focus more heavily on third party developers who wish 
  to use the Golem network for their own purposes.

You can find the Golem token contract at 
0xa74476443119A942dE498590Fe1f2454d7D4aC0d. The contract is fairly active with 
transactions occurring nearly every minute. The token itself is a standard ERC20
token and exhibits nothing too novel as a token in and of itself.

You can find all of the Ethereum raised during the Golem ICO at 
0x7da82C7AB4771ff031b66538D2fB9b0B047f6CF9. No money has been siphoned out and 
sold out of the 373,000 ethereum accrued. 
# Mechanics of Golem
  - How is Golem mined?

    Golem is not "mined" in the conventional sense as seen in Bitcoin and 
    Ethereum; rather, in order to receive GNT, users must lease out their 
    processing power to the network and allow others to rent out the allocated
    processing power. Lenders who place their processing power onto the network
    for others to use can be runemerated for their work through different 
    payment plans, but this has yet to be implemented.
    
  - Proof of Work?

    While the Golem transactions themselves are based around the Ethereum miners
    verifying the veracity of the transactions, the Proof of Work for the Golem 
    network itself is determining if the leaser, the one who is lending out 
    his/her processing power properly outputted the correct product (e.g A 
    leaser received a frame to render from someone looking to render 
    his/her computer animation; the proof of work is ensuring that the leaser
    properly rendered the frame in the expected manner or else the leaser won't 
    receive compensation for his work).
    
# Issues so far
- Unsecured and Unencrypted Data

  The data you send to other users in order for them to act upon it and process
  it to achieve the desired output will naturally be unsecured and unencrypted.
  As a result, there is barrier of entry for new users as requestors, those 
  looking to complete a task, will be reluctant to send data to unverified
  users who have not proven that they are reliable nodes.
  
- Verifying products

  Verifying that a leaser has properly completed the portion of the task given to him and has
  created the desired product is still relatively unclear on how to achieve.
  Currently, Golem uses peer-to-peer verification, wherein users complete the
  same task and compare their outputted products to ensure that they all 
  achieved the same output, to prevent malicious leasers from creating bogus 
  outputs while still receiving compensation for their work. However, This 
  method of peer verification is still susceptible to the problems plaguing  
  consensus, a problem that is compounded with the fact that relatively smaller 
  groups of leasers will be working on one task as opposed to what we see in
  Ethereum and Bitcoin. 
  
  The Golem team also proposes another measure to prevent the actions of 
  malicious leasers known as probabilistic verification, where the leaser's own
  machine calculates a small portion of the output and compares it with the end
  product the leaser has created to ascertain the veracity of the product. 
  Although probabilistic verification is sufficient for the graphicl rendering
  verification problem, for other use cases such as machine learning where you
  cannot solve only a "portion" of the training, probabilistic verification is
  rather useless.
  
- Economic Competition and volatility of a cryptocurrency

Golem is already competing with a myriad of massive and highly succesful businesses that
have already succesfully created relatively affordable public cloud computing services 
for the general public, such as Amazon and Microsoft. Golem hopes to be even cheaper and more inexpensive than
what is currently available, but that remains to be seen as Golem has yet to be 
fully released. There is currently little incentive to use the Golem network, 
which as stated above still contains problems that need to be addressed,
over AWS (Amazon Web Services) and Microsoft Azure which do not encounter the same
issues as the Golem network.

Since Golem is a cryptocurrency which is highly volatile, the price of a completing
a task is extremely ambigious and uncertain as the cost is based in GNT and not
USD. People who are seeking to have their task executed will not know how much
a certain amount of GNT will achieve as prices can potentially fluctuate wildly. 
$100 will get you a set amount of work and time with Azure and AWS; $100 worth of
GNT will not give you the same assurances since leasers will constantly be changing
their cost of labor. 

# Overall View

Golem simultaneously has a lot of potential and a lot of flaws. While some of these
deficiencies are daunting, the idea behind Golem, a distributed processing network
where anyone can join in and participate is extremely enticing. 
    
    